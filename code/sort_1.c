/*
*方法一：根据老师思路，自行编写的
*感觉不够通用性，而且程序有点长，不容易背
*所以注释就不写了
*/
#include <stdio.h>

int main(void){

	int i,j;
	int new_score;

	int score[50] = {60,82,55,40,96};

	for(i=0;i<4;i++){
		for(j=i+1;j<5;j++){
			if(score[i]<score[j]){
				temp = score[i];
				score[i] = score[j];
				score[j] = temp;
			}				
		}
	}

	for(i=0;i<5;i++) printf("%d\t",score[i]);
	
	printf("\ninput new score:");
	scanf("%d",&new_score);

	if(new_score <= score[4]){
		score[5] = new_score;

	}else if(new_score >= score[0]){

		for(i=5;i>0;i--){
			score[i] = score[i-1];
		}

		score[0] = new_score;
	
	}else{
		for(i=0;i<5;i++){
			if(new_score<score[i] && 
				new_score >= score[i+1]){
				for(j=6;j>i;j--){

					score[j] = score[j-1];
				}
				score[i+1] = new_score;
			}
		}
	}

	for(i=0;i<6;i++) printf("%d\t",score[i]);
	printf("\n");

	return 0;
}