/*
*方法二
*
*/
#include <stdio.h>

//冒泡排序
void sort(int *s, int len){

	int i;
	int j;
	int temp;

	for(i=0;i<len-1;i++){

		for(j=i+1;j<len;j++){
			//此为升序(大到小)排序，把下面if语句的 < 改为 > 即可变为降序（小到大）
			if(s[i] < s[j]){
				temp = s[i];
				s[i] = s[j];
				s[j] = temp;
			}				
		}
	}

}

int main(void){

	int i;
	int new_score;
	
	int score[50] = {60,82,55,40,96};

	// 进行排序，第一个参数为数组名，第二个参数为数组的长度（即多少个分数，示例为5个）
	sort(score,5); 
	for(i=0;i<5;i++) printf("%d\t",score[i]);

	// 输入新的分数
	printf("\nInput new score:");
	scanf("%d",&new_score);

	score[5] = new_score; 
	// 因为增加了一个分数，所有这里是为5+1 = 6 
	sort(score,6);

	for(i=0;i<6;i++) printf("%d\t",score[i]);
	printf("\n");

	return 0;
}